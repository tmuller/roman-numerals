class RomanNumerals {

  charValueObj = {
    M: 1000,
    D: 500,
    C: 100,
    L: 50,
    X: 10,
    V: 5,
    I: 1,
  };

  sequenceCount = {
    previousChar: '',
    count: 0
  }

  convert(romanNumberString) {
    if (romanNumberString.length < 1) return 0;

    const numberLetters = romanNumberString.split('');
    return numberLetters.reduce(this.addNumbers(numberLetters).bind(this), 0);
  }

  addNumbers(letterArray) {
    return (acc, currentLetter, currentId) => {
      this.manageSequenceCount(currentLetter);
      const nextLetter = letterArray[currentId + 1]
      const valueToAdd = this.getValueToAdd(currentLetter, nextLetter);
      this.sequenceCount.previousChar = currentLetter;
      return acc + valueToAdd;
    }
  }

  manageSequenceCount(currentLetter) {
    this.sequenceCount.count++;

    if (this.sequenceCount.previousChar !== currentLetter) this.sequenceCount.count = 1;
    else if (this.sequenceCount.count > 3) throw Error('Invalid format');
    else if (this.sequenceCount.count > 1 && ['L', 'D'].indexOf(currentLetter) > -1) throw Error('No multiples of "L" or "D"')
  }

  getValueToAdd(currentLetter, nextLetter) {
    if (!this.charValueObj[currentLetter]) throw new Error('Invalid character encountered');
    const nextLetterValue = this.charValueObj[nextLetter] || 0;
    const currentLetterValue = this.charValueObj[currentLetter] || 0;
    const factor = (!!nextLetterValue && nextLetterValue > currentLetterValue) ? -1 : 1;

    return factor * currentLetterValue;
  }

}

module.exports = RomanNumerals;
