const RomanNumerals = require('../src/RomanNumerals');

describe('Roman Numerals', () => {
  describe('convert()', () => {

    let classUnderTest;

    const expectExceptionFor = (romanNumber) => {
      const testFct = () => {
        const result = classUnderTest.convert(romanNumber);
      };
      expect(testFct).toThrow();
    };


    beforeEach(() => {
      classUnderTest = new RomanNumerals();
    });


    it('should return 0 for an empty string', () => {
      expect(classUnderTest.convert('')).toEqual(0);
    });

    it('should return 1000 for an "M"', () => {
      const romanNumber = 'M';
      expect(classUnderTest.convert(romanNumber)).toEqual(1000);
    });

    it('should multiply a number by the letter count', () => {
      const romanNumber = 'MM';
      expect(classUnderTest.convert(romanNumber)).toEqual(2000);
    });

    it('should throw an error for a series of 4 or more identical letters', () => {
      const romanNumber = 'MMMM';
      expectExceptionFor(romanNumber);
    });

    it('should interpret and add the number for "C"', () => {
      const romanNumber = 'MC';
      expect(classUnderTest.convert(romanNumber)).toEqual(1100);
    });

    it('should not permit more than three characters of one kind in a row ', () => {
      const romanNumber = 'MMCCCC';
      expectExceptionFor(romanNumber);
    });

    it('should subtract when next character is for a bigger value', () => {
      const romanNumber = 'MCM';
      expect(classUnderTest.convert(romanNumber)).toEqual(1900);
    });

    it('should correctly translate a year with four same chars, separated by another', () => {
      const romanNumber = 'MCMLXXXIX';
      expect(classUnderTest.convert(romanNumber)).toEqual(1989);
    });

    it('should throw an exception if an invalid character is contained', () => {
      const romanNumber = 'MCMHD';
      expectExceptionFor(romanNumber);
    });

    it('should translate the max repeated characters', () => {
      const romanNumber = 'MMMDCCCLXXXVIII';
      expect(classUnderTest.convert(romanNumber)).toEqual(3888);
    });

    it('should throw an exception for repeated "L" chars', () => {
      const romanNumber = 'MCLLX';
      expectExceptionFor(romanNumber);
    });

    it('should throw an exception for repeated "D" chars', () => {
      const romanNumber = 'MDDMCLX';
      expectExceptionFor(romanNumber);
    });

  });
});
